use AVTO_FIX

create table Cars(
id_car int identity(1,1) not null primary key,
car_make nvarchar(20) not null,
car_number nvarchar(20) not null,
year_of_release date not null,
owner_firstname nvarchar(20) not null,
owner_secondname nvarchar(20) not null,
owner_surname nvarchar(20) not null,
)

create table Mechanic(
id_mechanic int identity(1,1) not null primary key,
mechanic_firstname nvarchar(20) not null,
mechanic_secondname nvarchar(20) not null,
mechanic_surname nvarchar(20) not null,
qualification nvarchar(20),
premium money,
)

create table Nariad(
id_nariad int identity(1,1) not null primary key,
car_id int foreign key references Cars(id_car),
mechanic_id int foreign key references Mechanic(id_mechanic),
price money not null,
day_of_issue date not null,
work_type nvarchar(50) not null,
date_of_ending_plan date not null,
date_of_ending_real date
)

INSERT INTO Cars (car_make, car_number, year_of_release, owner_firstname, owner_secondname, owner_surname) VALUES
 ('BMW', 'AK1234AK', '2000', 'Max', 'Maximov', 'Maximchenko'),
 ('KIA', 'AM2323AM', '2996', 'Oleg', 'Olegovich', 'Olechka'),
 ('Citroen', 'AK1111AK', '2016', 'Vovan', 'Vladimirovich', 'Volchek'),
 ('Mini', 'HA777CH', '1960', 'Artyr', 'Ykralovich', 'Kon');

 INSERT INTO Mechanic (mechanic_firstname, mechanic_secondname, mechanic_surname, premium, qualification) VALUES
 ('Andrey', 'Andreevich', 'Andreenko', 2000, 'jun mechanic'),
 ('Anton', 'Antonovich', 'Antonenko', 4000, 'mid mechanic'),
 ('Ivan', 'Ivanovich', 'Ivanov', 6000, 'sin mechanic'),
 ('Sergey', 'Sergeevich', 'Serui', 2500, 'jun mechanic');

 INSERT INTO Nariad (car_id, mechanic_id, price, day_of_issue, work_type, date_of_ending_plan, date_of_ending_real) VALUES
  (1,1,10000,'2005-08-09','motor fix', '2005-06-09', '2005-06-10'),
 (2,2,5000,'2008-02-03','wheel repair', '2008-02-20', '2008-02-25'),
 (3,3,7000,'2018-11-11','wheel repair', '2018-11-27', '2018-12-01'),
 (4,4,12000,'2019-02-16','bumper repair', '2019-03-05', '2019-03-12');


create table Test5(
id_task int identity(1,1) not null primary key,
table_name nvarchar(20),
number int,
)
DROP TABLE Test5
--1
CREATE PROC AddRowNumber AS 
BEGIN
INSERT INTO Test5 (table_name, number) 
SELECT
    'Cars'tablename,
     COUNT(*) rows
FROM
    Cars
UNION
SELECT
    'Mechanic'tablename,
     COUNT(*) rows
FROM
    Mechanic
UNION
SELECT
    'Nariad'tablename,
     COUNT(*) rows
FROM
    Nariad;
END;
	
EXEC AddRowNumber
--2
CREATE PROC AddColNumber AS 
BEGIN
INSERT INTO Test5 (table_name, number) 
SELECT 'Cars'tablename, count(*) AS NUMBEROFCOLUMNS FROM information_schema.columns
     WHERE table_name ='Cars'
UNION
SELECT 'Mechanic'tablename, count(*) AS NUMBEROFCOLUMNS FROM information_schema.columns
     WHERE table_name ='Mechanic'
UNION
SELECT 'Nariad'tablename, count(*) AS NUMBEROFCOLUMNS FROM information_schema.columns
     WHERE table_name ='Nariad';
END;
	
EXEC AddColNumber
--3
CREATE PROC AddUnicColNumber AS 
BEGIN
set nocount on
select table_name, column_name, count(distinct column_name) as distinct_values
from information_schema.columns group by table_name,column_name
order by table_name
END;	
EXEC AddUnicColNumber
--Triggers
--A1
create trigger trigger1
on Cars 
after insert
as
insert into Nariad (car_id, mechanic_id, price, day_of_issue, work_type, date_of_ending_plan, date_of_ending_real) VALUES
  (1,1,10000,'2005-08-09','motor fix', '2005-06-09', '2005-06-10')
  INSERT INTO Mechanic (mechanic_firstname, mechanic_secondname, mechanic_surname, premium, qualification) VALUES
 ('Andrey', 'Andreevich', 'Andreenko', 2000, 'jun mechanic')

 INSERT INTO Cars (car_make, car_number, year_of_release, owner_firstname, owner_secondname, owner_surname) VALUES
 ('BMW', 'AK1234AK', '2000', 'Max', 'Maximov', 'Maximchenko')
 --B1
 CREATE TRIGGER triggertestD
ON  Mechanic
FOR DELETE
AS 
BEGIN
DELETE FROM Nariad
    WHERE mechanic_id in (
    SELECT i.id_mechanic
    FROM deleted i 
    INNER JOIN Mechanic
    ON i.id_mechanic = Nariad.employeeID
    where i.employeeID = testTable.employeeID)
END

