

CREATE DATABASE library_sum
ON
(
	NAME = library_s_data, --logical name
	FILENAME = 'F:\Cource3_DEV\databases\sum1\library_s_data.mdf', -- physic location
	SIZE = 100, --max size in mb
	MAXSIZE = 250, --maximum file size
	FILEGROWTH = 10% --filegrowth with % 
)
LOG ON
(
	NAME = 'library_s_log', --logical log name
	FILENAME = 'F:\Cource3_DEV\databases\sum1\library_s_log.ldf', --physical location
	SIZE = 100MB,  --max size in mb
	FILEGROWTH = 10MB --filegrowth in mb 
)
GO

USE library_sum;

CREATE TABLE book
(
	id_b INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	name NVARCHAR(60) NOT NULL,
	author NVARCHAR(60) NOT NULL,
	p_count INT NOT NULL CONSTRAINT p_count_format CHECK (p_count>0),
	p_year DATE NOT NULL,
	price INT NOT NULL CONSTRAINT p_year_format CHECK (price > 0),
)

CREATE TABLE reader
(
	id_r INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	creds NVARCHAR(127) NOT NULL,
	adds NVARCHAR(127) NULL DEFAULT NULL,
	phone VARCHAR(10) CONSTRAINT phone_format CHECK (phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	identifier INT NOT NULL,
	graduate BIT NOT NULL DEFAULT 0,
)
ALTER TABLE reader ADD UNIQUE(identifier)

CREATE TABLE publish
(
	id_p INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	give_date DATE NOT NULL,
	real_date DATE NOT NULL,
	plan_date DATE NOT NULL,
	id_b INT FOREIGN KEY REFERENCES book(id_b) ON DELETE CASCADE ON UPDATE NO ACTION,
	id_r INT FOREIGN KEY REFERENCES reader(id_r) ON DELETE CASCADE ON UPDATE NO ACTION
)

INSERT INTO reader (creds, adds, phone, identifier, graduate) VALUES 
	('John Smith', 'green allay 7', '0950350949', 100,0),
	('Jack Lourance', 'baker street', '0668520949', 101,0),
	('Luna Loud', 'saint row 3', '0961550949', 102,0),
	('Joseph Lee', 'china town', '0675225949', 103,0)


INSERT INTO book (name, author, p_count, p_year, price) VALUES
	('Very good book', 'Jakie Dogood', 10, '2017-03-15', 107),
	('Bad blood', 'Cassy Brings', 10, '2018-07-21', 55),
	('Building a company', 'Joshua Holms', 10, '2017-01-19', 20),
	('Red blue and white', 'Jassie Watsons', 10, '2016-04-07', 110)


INSERT INTO publish (give_date, real_date, plan_date, id_b, id_r) VALUES 
	('2019-02-07', '2019-03-07', '2019-03-02', 1, 1),
	('2019-03-18', '2019-05-20', '2019-04-18', 2, 2),
	('2019-06-20', '2019-07-09', '2019-07-18', 3, 4)

SELECT * FROM book
SELECT * FROM publish
SELECT * FROM reader

