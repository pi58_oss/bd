use library_sum

-- ******** Procedures ********

-- number 1
CREATE PROCEDURE GetAllTablesRows AS
BEGIN
	CREATE TABLE #counts (table_name varchar(255), row_count int)
	EXEC sp_MSForEachTable @command1='INSERT #counts (table_name, row_count) SELECT ''?'', COUNT(*) FROM ?'
	SELECT table_name, row_count FROM #counts ORDER BY table_name, row_count DESC
	DROP TABLE #counts
END

EXEC GetAllTablesRows

-- number 2
CREATE PROC GetAllTablesCols AS
BEGIN
	CREATE TABLE #counts (table_name varchar(255), col_count int)

	INSERT INTO #counts
	SELECT  tab.table_name, COUNT(col.column_name)
	FROM INFORMATION_SCHEMA.TABLES tab
	JOIN INFORMATION_SCHEMA.COLUMNS col ON col.table_name = tab.table_name
	WHERE tab.table_type != 'VIEW'
	GROUP BY tab.table_name 
	ORDER BY tab.table_name
	SELECT * FROM #counts
	DROP TABLE #counts
END

EXEC GetAllTablesCols

-- number 3
CREATE PROC GetUniqueCountFromTable @tableName VARCHAR(20) AS 
BEGIN
	CREATE TABLE #counts (table_name VARCHAR(MAX))
	INSERT INTO #counts
	SELECT 'SELECT COUNT(DISTINCT [' + COLUMN_NAME  + ']) AS ' + COLUMN_NAME  + ' FROM ' + TABLE_NAME
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_NAME = 'book'

	DECLARE @query NVARCHAR(max);
	DECLARE cur CURSOR LOCAL FAST_FORWARD FOR
		(SELECT * FROM #counts)
	OPEN cur
	FETCH NEXT FROM cur INTO @query
	WHILE(@@FETCH_STATUS=0)
	BEGIN
		EXEC(@query)
		FETCH NEXT FROM cur INTO @query
	END
	CLOSE cur
	DEALLOCATE cur
	
	DROP TABLE #counts
END

EXEC GetUniqueCountFromTable 'book'


-- ******** TRIGGERS ********

-- !!! PLEASE DROP TRIGGER AFTER USE (IN CASE) TO AVOID FUNCTIONAL CONFLICTS !!!
-- Design: --[section]--(\n[listing]\n[test]\n\t[about])x2

-- ON UPDATE --

CREATE TRIGGER on_update_BookInfo ON publish AFTER UPDATE AS
DECLARE @pubId INT, @publishCount INT,
@give_date DATE, @real_date DATE, @plan_date DATE, @id_b INT, @id_r INT
DECLARE cur_d CURSOR FOR SELECT id_p, give_date, real_date, plan_date, id_b, id_r FROM deleted
OPEN cur_d
FETCH NEXT FROM cur_d INTO @pubId, @give_date, @real_date, @plan_date, @id_b, @id_r
WHILE @@FETCH_STATUS = 0 
BEGIN
	SELECT @publishCount = (
		SELECT COUNT(id_b) FROM publish WHERE DATEDIFF(DAY, GETDATE(), real_date) > 0 AND id_p = @pubId
	) 
	IF @publishCount > 0
	BEGIN
		UPDATE publish 
		SET give_date = @give_date, real_date = @real_date, plan_date = @plan_date, id_b = @id_b, id_r = @id_r 
		WHERE id_p = @pubId
	END
	FETCH NEXT FROM cur_d INTO @pubId, @give_date, @real_date, @plan_date, @id_b, @id_r
END 
CLOSE cur_d
DEALLOCATE cur_d

INSERT INTO publish (give_date, plan_date, real_date, id_b, id_r) 
VALUES (DATEADD(month, -1, GETDATE()), DATEADD(month, 1, GETDATE()), DATEADD(month, 2, GETDATE()), 1, 1) 
SELECT * FROM publish
UPDATE publish SET id_r = 3 WHERE id_p = 3 --will work
UPDATE publish SET id_r = 4 WHERE id_p = 3 --will fail
	-- (subs) // roll back if book is still associated with someone  _in cases if real_date was pre written_

CREATE TRIGGER on_update_BookId ON book AFTER UPDATE AS
DECLARE @bookId INT, @bookId_i INT, @pubId INT
DECLARE cur_d CURSOR FOR SELECT id_b FROM deleted
DECLARE cur_i CURSOR FOR SELECT id_b FROM inserted
OPEN cur_d
OPEN cur_i
FETCH NEXT FROM cur_d INTO @bookId
FETCH NEXT FROM cur_i INTO @bookId_i
WHILE @@FETCH_STATUS = 0 
BEGIN
	DECLARE cur_publish CURSOR FOR SELECT id_p FROM publish WHERE id_b = @bookId
	OPEN cur_publish
	FETCH NEXT FROM cur_publish INTO @pubId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		UPDATE publish SET id_b = @bookId_i WHERE id_p = @pubId
		FETCH NEXT FROM cur_publish INTO @pubId	
	END
	CLOSE cur_publish
	DEALLOCATE cur_publish
	FETCH NEXT FROM cur_d INTO @bookId
END
CLOSE cur_d 
CLOSE cur_i
DEALLOCATE cur_d
DEALLOCATE cur_i

SELECT * FROM book 
SELECT * FROM publish
UPDATE book SET id_b = 12 WHERE id_b = 1 -- _to make it work uncheck 'identity' in Design->IdentytySpecification_
	-- (subs) // change book_id everywhere if it was changed in `book` table

-- ON DELETE --

CREATE TRIGGER on_delete_Book ON book FOR DELETE AS
DECLARE @bookId INT
DECLARE cur_d CURSOR FOR SELECT id_b FROM deleted
OPEN cur_d
FETCH NEXT FROM cur_d INTO @bookId
WHILE @@FETCH_STATUS = 0 
BEGIN
	DELETE FROM publish WHERE id_b = @bookId
	FETCH NEXT FROM cur_d INTO @bookId
END
CLOSE cur_d 
DEALLOCATE cur_d

SELECT * FROM book
SELECT * FROM publish
DECLARE @lastBookId INT
INSERT INTO book (name, author, p_count, p_year, price) 
VALUES ('Text book', 'Some Author', 10, GETDATE(), 14)
SELECT @lastBookId = (SELECT TOP 1 id_b FROM book ORDER BY id_b DESC)
INSERT INTO publish (give_date, plan_date, real_date, id_b, id_r) 
VALUES (DATEADD(month, -1, GETDATE()), GETDATE(), DATEADD(month, 1, GETDATE()), @lastBookId, 2) 
DELETE book WHERE id_b = @lastBookId
	-- (subs) // remove all publishes of deleted reader_id

CREATE TRIGGER on_prevent_delete_Publish ON publish FOR DELETE AS
DECLARE @id_p INT, @give_date DATE, @real_date DATE, @plan_date DATE, @id_b INT, @id_r INT
SET IDENTITY_INSERT publish ON
DECLARE cur_d CURSOR FOR SELECT id_p, give_date, real_date, plan_date, id_b, id_r FROM deleted
OPEN cur_d
FETCH NEXT FROM cur_d INTO @id_p, @give_date, @real_date, @plan_date, @id_b, @id_r
WHILE @@FETCH_STATUS = 0 
BEGIN
	IF ( (SELECT COUNT(id_b) FROM book WHERE id_b = @id_b) > 0 )
	BEGIN
		INSERT INTO publish (id_p, give_date, real_date, plan_date, id_b, id_r)
		VALUES (@id_p, @give_date, @real_date, @plan_date, @id_b, @id_r)
	END
	FETCH NEXT FROM cur_d INTO @id_p, @give_date, @real_date, @plan_date, @id_b, @id_r
END
SET IDENTITY_INSERT publish OFF
CLOSE cur_d 
DEALLOCATE cur_d
-- use insert from previous trigger (from declare to last insert) for this "test case"
SELECt * FROM publish
DELETE publish WHERE id_p = 7 --fill fall(roll) back to previous
	-- (subs) // if trying to delete publish but book exists, than WILL FAIL! 

-- ON INSERT -- 

-- didn't understand the task (what it meens)
-- will create trigger that makes insertion into the other one table while creating new reader
SELECT * FROM reader
CREATE TABLE reader_optional (
	id_r INT NOT NULL FOREIGN KEY REFERENCES reader(id_r) ON DELETE CASCADE ON UPDATE NO ACTION,
	join_date DATE NULL DEFAULT CURRENT_TIMESTAMP, 
	membership BIT NULL DEFAULT 0,
	adventercy NVARCHAR(15) NULL DEFAULT 'normal' -- MySQL analog? ENUM('normal', 'increased', 'strict')
)

CREATE TRIGGER on_insert_Reader ON reader AFTER INSERT AS
DECLARE @id_r INT
DECLARE cur_i CURSOR FOR SELECT id_r FROM inserted
OPEN cur_i
FETCH NEXT FROM cur_i INTO @id_r
WHILE @@FETCH_STATUS = 0 
BEGIN
	INSERT INTO reader_optional (id_r, join_date, membership, adventercy)
	VALUES (@id_r, GETDATE(), 0, 'normal')
	FETCH NEXT FROM cur_i INTO @id_r
END
CLOSE cur_i
DEALLOCATE cur_i

SELECT * FROM reader
SELECT reader.*,reader_optional.* FROM reader_optional JOIN reader ON reader.id_r = reader_optional.id_r
INSERT INTO reader (creds, adds, phone, identifier, graduate) 
VALUES ('Test Reader One', 'Test Address 1/2', '0501234567', 111, 0)
	-- (subs) if new reader joined, than fill in some optional info about him :) [imagination problem] 

-- cursors where used for massive operation (like massive insert, update, delete ...)